# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.1.0](https://gitlab.com/AmilcarM11/test-sem-ver/compare/v0.0.0...v0.1.0) (2022-05-30)


### Features

* **dev tools:** add git-hook to lint commit msgs ([1f5261f](https://gitlab.com/AmilcarM11/test-sem-ver/commit/1f5261f6fd66b4b9a7866cc3e18a9d4bf527e181))
* **dev tools:** add husky install to npm's postinstall ([0beb68f](https://gitlab.com/AmilcarM11/test-sem-ver/commit/0beb68f9283d05ac7cf4a488780ea04df676ed5d))
* **dev tools:** definir tipo de version-bump ([7c6ddb2](https://gitlab.com/AmilcarM11/test-sem-ver/commit/7c6ddb2f3f9fa5bc85643bc77d0ae7a595ca7736))

## 0.0.0 (2022-05-30)


### Features

* **dev tools:** add 'standard-version' library ([5344d6f](https://gitlab.com/AmilcarM11/test-sem-ver/commit/5344d6fe2449ed7a20d5175dbbeac23e994076ce))
